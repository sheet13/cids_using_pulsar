# CIDS_Using_Pulsar

Code base for scripts enabling Collaborative Intrusion Detection System using Pulsar

---

## check_bw_final.py

Scans the bandwidth usage on all the ports and when the usage goes beyond a threshold, sends the IP onto a Pulsar Topic

---

## sniff-traffic.py

Scans all the ports using scapy to sniff all traffic (to and fro) between the hosts connected to the network.
The detected traffic would be loaded to a pulsar topic

---

## consumer-load.py

This script takes the data off the pulsar topic posted to by the sniff-traffic script and loads it to a table, which is transactional in nature.
This way we prevent having to hit the Database too often.

---

## consumer-stat-capture.py

This script takes the data of the transactional table periodically and aggregates them to calcuate: 

* number of packets sent from a particular host
* number of packets sent to a particular host

If the number of packets sent > threshold --> Send the IP to the same Pulsar topic as "check_bw_final.py"
If the number of packets received > threshold --> Send the top 3 IP to the same Pulsar topic as "check_bw_final.py"

---

## post-acl-rules.py / post-acl-rules-nw2.py

This script would take the data of the Pulsar topic and would add the ACL rules on their respective controllers and add an entry to a table with times tamp.

---

## acl-rules-housekeeping.py

This script ensures no host is permanently blocked on a network.
After a banishment of a stipulated time, the hosts would be unblocked by this script.

---
