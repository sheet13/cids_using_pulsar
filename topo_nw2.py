#!/usr/bin/python

from mininet.net import Mininet
from mininet.node import Controller, RemoteController, OVSController
from mininet.node import CPULimitedHost, Host, Node
from mininet.node import OVSKernelSwitch, UserSwitch
from mininet.node import IVSSwitch
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import TCLink, Intf
from subprocess import call
from os import environ
"""Custom topology example

Two directly connected switches plus a host for each switch:

   host --- switch --- switch --- host

Adding the 'topos' dict with a key/value pair to generate our newly defined
topology enables one to pass in '--topo=mytopo' from the command line.
"""

from mininet.topo import Topo

collector = environ.get('COLLECTOR','127.0.0.1')
sampling = environ.get('SAMPLING','10')
polling = environ.get('POLLING','10')

class MyTopo( Topo ):
    "Simple topology example."

    def __init__( self ):
        "Create custom topo."

        # Initialize topology
        Topo.__init__( self )

        # Add hosts and switches
        s21 = self.addSwitch('s21', cls=OVSKernelSwitch)
        s22 = self.addSwitch('s22', cls=OVSKernelSwitch)
        s23 = self.addSwitch('s23', cls=OVSKernelSwitch)
        s24 = self.addSwitch('s24', cls=OVSKernelSwitch)
        s25 = self.addSwitch('s25', cls=OVSKernelSwitch)
        h21 = self.addHost('h21', ip='10.0.1.1', defaultRoute=None, startCommand= 'sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.lo.disable_ipv6=1')
        h22 = self.addHost('h22', ip='10.0.1.2', defaultRoute=None, startCommand='sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.lo.disable_ipv6=1')
        h23 = self.addHost('h23', ip='10.0.1.3', defaultRoute=None, startCommand= 'sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.lo.disable_ipv6=1')
        h24 = self.addHost('h24', ip='10.0.1.4', defaultRoute=None, startCommand= 'sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.lo.disable_ipv6=1')
        h25 = self.addHost('h25', ip='10.0.1.5', defaultRoute=None, startCommand= 'sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.lo.disable_ipv6=1')
        h26 = self.addHost('h26', ip='10.0.1.6', defaultRoute=None, startCommand= 'sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.lo.disable_ipv6=1')
        h27 = self.addHost('h27', ip='10.0.1.7', defaultRoute=None, startCommand='sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.lo.disable_ipv6=1')
        h28 = self.addHost('h28', ip='10.0.1.8', defaultRoute=None, startCommand= 'sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.lo.disable_ipv6=1')
        h29 = self.addHost('h29', ip='10.0.1.9', defaultRoute=None, startCommand= 'sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.lo.disable_ipv6=1')
        h210 = self.addHost('h210', ip='10.0.1.10', defaultRoute=None, startCommand= 'sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.lo.disable_ipv6=1')
        h211 = self.addHost('h211', ip='10.0.1.11', defaultRoute=None, startCommand='sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.lo.disable_ipv6=1')

        # Add links
        self.addLink(s21, h21)
        self.addLink(s21, h22)
        self.addLink(s22, h23)
        self.addLink(s22, h24)
        self.addLink(s22, h25)
        self.addLink(s23, h26)
        self.addLink(s23, h27)
        self.addLink(s23, h28)
        self.addLink(s24, h29)
        self.addLink(s24, h210)
        self.addLink(s24, h211)
        self.addLink(s21, s25)
        self.addLink(s22, s25)
        self.addLink(s23, s25)
        self.addLink(s24, s25)

topos = { 'mytopo': ( lambda: MyTopo() ) }

