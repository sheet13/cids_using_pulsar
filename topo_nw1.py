#!/usr/bin/python

from mininet.net import Mininet
from mininet.node import Controller, RemoteController, OVSController
from mininet.node import CPULimitedHost, Host, Node
from mininet.node import OVSKernelSwitch, UserSwitch
from mininet.node import IVSSwitch
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import TCLink, Intf
from subprocess import call
from os import environ
"""Custom topology example

Two directly connected switches plus a host for each switch:

   host --- switch --- switch --- host

Adding the 'topos' dict with a key/value pair to generate our newly defined
topology enables one to pass in '--topo=mytopo' from the command line.
"""

from mininet.topo import Topo

collector = environ.get('COLLECTOR','127.0.0.1')
sampling = environ.get('SAMPLING','10')
polling = environ.get('POLLING','10')

class MyTopo( Topo ):
    "Simple topology example."

    def __init__( self ):
        "Create custom topo."

        # Initialize topology
        Topo.__init__( self )

        # Add hosts and switches
        s11 = self.addSwitch('s11', cls=OVSKernelSwitch)
        s12 = self.addSwitch('s12', cls=OVSKernelSwitch)
        h12 = self.addHost('h12', ip='10.0.0.2', defaultRoute=None, startCommand='sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.lo.disable_ipv6=1')
        h15 = self.addHost('h15', ip='10.0.0.5', defaultRoute=None, startCommand= 'sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.lo.disable_ipv6=1')
        h19 = self.addHost('h19', ip='10.0.0.9', defaultRoute=None, startCommand= 'sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.lo.disable_ipv6=1')
        h125 = self.addHost('h125', ip='10.0.0.25', defaultRoute=None, startCommand= 'sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.lo.disable_ipv6=1')
        h17 = self.addHost('h17', ip='10.0.0.7', defaultRoute=None, startCommand= 'sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.lo.disable_ipv6=1')
        h16 = self.addHost('h16', ip='10.0.0.6', defaultRoute=None, startCommand= 'sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.lo.disable_ipv6=1')
        h14 = self.addHost('h14', ip='10.0.0.4', defaultRoute=None, startCommand= 'sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.lo.disable_ipv6=1')
        h11 = self.addHost('h11', ip='10.0.0.1', defaultRoute=None, startCommand= 'sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.lo.disable_ipv6=1')
        h13 = self.addHost('h13', ip='10.0.0.3', defaultRoute=None, startCommand= 'sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.lo.disable_ipv6=1')
        h18 = self.addHost('h18', ip='10.0.0.8', defaultRoute=None, startCommand= 'sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.default.disable_ipv6=1;sysctl -w net.ipv6.conf.lo.disable_ipv6=1')

        # Add links
        self.addLink(s11, h11)
        self.addLink(s11, h12)
        self.addLink(s11, h13)
        self.addLink(s11, h14)
        self.addLink(s11, h15)
        self.addLink(s12, h16)
        self.addLink(s12, h17)
        self.addLink(s12, h18)
        self.addLink(s12, h19)
        self.addLink(s12, h125)
        self.addLink(s11, s12)

topos = { 'mytopo': ( lambda: MyTopo() ) }

